.SUFFIXES: .html .adoc

.adoc.html:
	asciidoc $<

SOURCES = $(shell ls *.adoc)

all: $(SOURCES:.adoc=.html)

clean:
	rm -f *.html

upload: all
	cd ..; upload loadsharers/*.html

